use std::{
    convert::Infallible,
    error::Error,
    net::SocketAddr,
    path::{self, PathBuf},
    sync::Arc,
    time::{Duration, Instant},
};

use axum::{
    extract::{Host, State},
    http::{uri::PathAndQuery, StatusCode, Uri},
    response::Response,
    routing::get,
    Router,
};
use mimalloc::MiMalloc;
use regex::Regex;
use serde::Deserialize;
use tokio::sync::RwLock;
use tracing::{debug, error, info, warn};
use tracing_subscriber::{fmt::format, EnvFilter};

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    // Initialize logging
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    tracing_subscriber::fmt()
        .event_format(format().compact())
        .with_env_filter(env_filter)
        .init();

    // Log service start.
    info!(
        "Starting redirects service version {}.",
        env!("CARGO_PKG_VERSION")
    );

    // Load environment variables
    let port = if let Ok(Ok(port)) = std::env::var("PORT").map(|s| s.trim().parse::<u16>()) {
        port
    } else {
        warn!("Couldn't parse PORT environment variable. Using default: 3000.");
        3000
    };
    let redirects_toml =
        if let Ok(rf) = std::env::var("REDIRECTS_TOML").map(|rf| Arc::new(PathBuf::from(rf))) {
            rf
        } else {
            warn!(
                "Environment variable REDIRECTS_TOML not provided. Using default: \
                \"config/redirects.toml\"."
            );
            Arc::new("config/redirects.toml".into())
        };
    let reload_interval = Duration::from_secs(
        if let Ok(Ok(ri)) = std::env::var("RELOAD_INTERVAL").map(|ri| ri.parse::<u64>()) {
            ri
        } else {
            warn!("Couldn't parse RELOAD_INTERVAL environment variable. Using default: 10s.");
            10
        },
    );
    let retry_interval = Duration::from_secs(
        if let Ok(Ok(ri)) = std::env::var("RETRY_INTERVAL").map(|ri| ri.parse::<u64>()) {
            ri
        } else {
            warn!("Couldn't parse RETRY_INTERVAL environment variable. Using default: 3s.");
            3
        },
    );
    let throw_errors = std::env::var("THROW_ERRORS")
        .ok()
        .and_then(|se| match se.trim().to_ascii_lowercase().as_ref() {
            "y" | "yes" | "on" | "true" | "1" => Some(true),
            "n" | "no" | "off" | "false" | "0" => Some(false),
            _ => None,
        })
        .unwrap_or_else(|| {
            warn!("Couldn't parse THROW_ERRORS environment variable. Using default: false.");
            false
        });

    // Initialize redirect rules.
    let redirects = Arc::new(RwLock::new(Redirects::default()));
    if let Err(e) =
        reload_redirects(&redirects, &redirects_toml, Duration::ZERO, retry_interval).await
    {
        warn!("Initial loading of the redirects file failed.");
        redirects.write().await.error = Some(e.to_string());
    }

    let state = WebState {
        redirects_toml,
        redirects,
        reload_interval,
        retry_interval,
        throw_errors,
    };

    // build our application with a single fallback route that sends all
    // requests to the `handler` function.
    let app = Router::new().fallback(get(handler)).with_state(state);

    // Run our app with axum, listening globally on the specified port.
    info!("Listening on port {port}.");
    let socket_addr = SocketAddr::from(([0, 0, 0, 0], port));
    let tcp_listener = tokio::net::TcpListener::bind(socket_addr).await?;
    axum::serve(tcp_listener, app).await.map_err(Into::into)
}

#[derive(Clone)]
struct WebState {
    redirects_toml: Arc<PathBuf>,
    redirects: Arc<RwLock<Redirects>>,
    reload_interval: Duration,
    retry_interval: Duration,
    throw_errors: bool,
}

async fn handler(
    Host(host): Host,
    uri: Uri,
    State(state): State<WebState>,
) -> Result<Response<String>, Infallible> {
    // Reload if interval has passed.
    let elapsed_since_reload = state.redirects.read().await.elapsed_since_reload();
    debug!("Elapsed since reload: {elapsed_since_reload:?}");
    let error_response = |e: String| {
        Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body(e)
            .expect("Is a valid response")
    };
    if elapsed_since_reload >= state.reload_interval {
        info!("Reloading redirects.");
        if let Err(e) = reload_redirects(
            &state.redirects,
            &state.redirects_toml,
            state.reload_interval,
            state.retry_interval,
        )
        .await
        {
            if state.throw_errors {
                return Ok(error_response(e.to_string()));
            }
        }
    } else if state.throw_errors {
        if let Some(e) = &state.redirects.read().await.error {
            return Ok(error_response(e.clone()));
        }
    }

    // Reverse iteration in order to start with higher priorities.
    for redirect in state.redirects.read().await.redirects.iter().rev() {
        if let Some(regex) = &redirect.source.0 {
            let Some(uri) = uri.path_and_query().map(PathAndQuery::as_str) else {
                continue;
            };

            if let Some(captures) = regex.captures(uri) {
                let mut destination = String::new();
                captures.expand(&redirect.destination, &mut destination);
                info!(
                    "Redirecting request from \"{}\" with uri \"{uri}\" to \"{}\".",
                    host, destination,
                );
                return Ok(Response::builder()
                    .status(StatusCode::TEMPORARY_REDIRECT)
                    .header("Location", destination)
                    .body(String::new())
                    .expect("Is a valid response"));
            }
        }
    }

    warn!("No redirect found for uri: {uri}.");

    Ok(Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body("404: Not Found".into())
        .expect("Is a valid response"))
}

async fn reload_redirects(
    redirects: &RwLock<Redirects>,
    redirects_toml: &path::Path,
    reload_interval: Duration,
    retry_interval: Duration,
) -> Result<(), Box<dyn std::error::Error>> {
    async fn get_new_redirects(
        redirects: &mut Redirects,
        redirects_toml: &path::Path,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // Compute checksum in order to skip parsing and sorting redirects if the
        // file didn't change.
        let current_md5sum = redirects.md5sum;

        debug!("Is this even called?");

        *redirects = match tokio::fs::read_to_string(redirects_toml).await {
            Ok(redirects_toml) => {
                let md5sum = md5::compute(redirects_toml.as_bytes()).into();
                if md5sum == current_md5sum {
                    info!("Skipping reload, because the file hasn't been changed.");
                    return if let Some(e) = redirects.error.clone() {
                        Err(e.into())
                    } else {
                        redirects.reloaded();
                        Ok(())
                    };
                }
                debug!("Checksum changed, trying to reload the config file.");
                // When deserializing a new `Redirects`, the `last_reload` field
                // is automatically set to `Instant::now()` and `error` is set
                // to `None`.
                match toml::from_str::<Redirects>(&redirects_toml) {
                    Ok(mut new_redirects) => {
                        debug!("Successfully loaded new config file.");
                        new_redirects.redirects.sort_unstable_by_key(|r| r.priority);
                        new_redirects.md5sum = md5sum;
                        new_redirects
                    }
                    Err(e) => {
                        error!("Deserialization failed: {e}.");
                        redirects.md5sum = md5sum;
                        return Err(e.into());
                    }
                }
            }
            Err(e) => {
                error!("Reading file failed: {e}.");
                return Err(e.into());
            }
        };

        Ok(())
    }

    // Acquire exclusive lock before accessing file to minimize file system
    // access.
    let mut redirects_writable = redirects.write().await;

    // Check if the interval has been reset by a different task since we tried
    // to aquire the write lock.
    if redirects_writable.last_reload.elapsed() < reload_interval {
        return if let Some(e) = redirects_writable.error.clone() {
            Err(e.into())
        } else {
            Ok(())
        };
    }

    if let Err(e) = get_new_redirects(&mut redirects_writable, redirects_toml).await {
        // Wait for `retry_interval` until the next try to prevent unnecessary
        // file system contention.
        redirects_writable.retry(reload_interval, retry_interval);
        // Store the error, so it can be returned in case the file didn't change
        // at the next attempt.
        redirects_writable.error = Some(e.to_string());
        Err(e)
    } else {
        Ok(())
    }
}

#[derive(Debug, Deserialize)]
struct Redirects {
    redirects: Vec<Redirect>,
    #[serde(skip, default = "Instant::now")]
    last_reload: Instant,
    #[serde(skip, default)]
    md5sum: [u8; 16],
    #[serde(skip, default)]
    error: Option<String>,
}

impl Redirects {
    fn retry(&mut self, reload_interval: Duration, retry_interval: Duration) {
        self.last_reload = Instant::now()
            .checked_sub(reload_interval)
            .map_or_else(Instant::now, |instant| instant + retry_interval);
    }

    fn reloaded(&mut self) {
        self.last_reload = Instant::now();
    }

    /// `self.last_reload` might be in the future (due to the retry mechanism).
    /// Since `Instant::elapsed` might panic in that case, we need to check
    /// manually if that is the case.
    fn elapsed_since_reload(&self) -> Duration {
        let now = Instant::now();
        if now > self.last_reload {
            self.last_reload.elapsed()
        } else {
            Duration::ZERO
        }
    }
}

impl Default for Redirects {
    fn default() -> Self {
        Self {
            redirects: vec![],
            last_reload: Instant::now(),
            md5sum: Default::default(),
            error: None,
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
struct Redirect {
    source: Source,
    destination: String,
    #[serde(default)]
    priority: i32,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(from = "String")]
struct Source(Option<Regex>);

impl From<String> for Source {
    fn from(s: String) -> Self {
        match Regex::new(&s) {
            Ok(regex) => Source(Some(regex)),
            Err(e) => {
                error!("Compiling regex failed for \"{s}\": {e}.");
                Source(None)
            }
        }
    }
}
