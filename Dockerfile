####################################################################################################
## Builder
####################################################################################################
FROM rust:slim AS builder

RUN update-ca-certificates

# Create appuser
ENV USER=user
ENV UID=1000

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /build

COPY ./ .

RUN cargo build --release

####################################################################################################
## Final image
####################################################################################################
FROM gcr.io/distroless/cc-debian12

# Import from builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

WORKDIR /service

# Copy our build
COPY --from=builder /build/target/release/redirects ./

# Use an unprivileged user.
USER user:user

# Define environment
ARG RELOAD_INTERVAL
ARG REDIRECTS_TOML
ARG PORT
ARG RUST_LOG

CMD ["/service/redirects"]
