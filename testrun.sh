#!/bin/sh

RUST_LOG=debug REDIRECTS_TOML=config/redirects.toml RELOAD_INTERVAL=10 RETRY_INTERVAL=3 THROW_ERRORS=y cargo r
